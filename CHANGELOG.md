# Change Log

## 1.0.4 (Sep 3, 2018)

* Fix auto indentation for comments block. Both types of comments indentation was fixed.

## 1.0.3 (Sep 16, 2017)

* Fix c++ comments.

## 1.0.2 (Sep 11, 2017)

* Add generic class support for C#.
* Fix empty method comments for C#.
* Remove enter command for C# comments.

## 1.0.0 (Sep 7, 2017)

* Start repo.
